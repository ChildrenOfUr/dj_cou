from collections import namedtuple

TeamMember = namedtuple("TeamMember", "slug name")

team = [
    # Column 1:
    [
        TeamMember("andy", "Andy Castille"),
        TeamMember("katharine", "Katharine Gibeau"),
        TeamMember("paul", "Paul VanKeuren"),
        TeamMember("pixley", "Craig Cottingham"),
    ],
    # Column 2:
    [
        TeamMember("forrest", "Forrest James"),
        TeamMember("christine", "Christine"),
        TeamMember("develle", "Laura Montgomery"),
    ],
    # Column 3:
    [
        TeamMember("robert", "Robert McDermot"),
        TeamMember("courtney", "Courtney B Reid"),
        TeamMember("lizzard", "Liz Henry"),
    ],
]
