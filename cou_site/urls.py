from django.urls import path

from . import views

urlpatterns = [
    path("", views.home_page, name="home"),
    path("about", views.about_page, name="about"),
    path("about/features", views.features_page, name="features"),
    path("notes", views.notes_page, name="notes"),
    path("help", views.help_page, name="help"),
    path("fine-print", views.fine_print_page, name="fine-print"),
    path("guide", views.guides_page, name="guides")
]
