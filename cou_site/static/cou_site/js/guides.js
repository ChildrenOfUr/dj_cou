$(document).ready(function () {
    // Download JSON
    $.get("https://server.childrenofur.com:8181/elevation/list/guide", function (guides) {
        // Remove guides who have never logged in
        let haveLoggedIn = [];
        $.each(guides, function (i, user) {
            if (user.last_login !== "never") {
                haveLoggedIn.push(user);
            }
        });

        // Sort by login date, with the most recent at the start
        let sorted = haveLoggedIn.sort(function (a, b) {
            let aL = new Date(a.last_login);
            let bL = new Date(b.last_login);
            return ((aL > bL) ? -1 : ((aL < bL) ? 1 : 0));
        });

        // Display the list
        $.each(sorted, function (i, user) {
            let link = '<a href="../profile/?username=' + user.username + '">' + user.username + '</a>';
            let rel = $.timeago(user.last_login);
            let item = '<li title="Last logged in ' + rel + '">' + link + '</li>';
            $("#guide-list").append(item);
        });
        // Hide "Become a Guide" section if you are logged in as one
        if (getCookie("loggedIn") !== "") {
            if ($.inArray(localStorage["username"], guides) !== -1) {
                $("#req-noguide").hide();
            }
        }
    });
});
