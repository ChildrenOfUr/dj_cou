from django.apps import AppConfig


class CouSiteConfig(AppConfig):
    name = "cou_site"
