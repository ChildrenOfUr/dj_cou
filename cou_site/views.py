from django.shortcuts import render

from .util.backgrounds import rand_bg
from .util.team import team


def home_page(request):
    return render(request, "index.html", context={"bg": rand_bg()})


def about_page(request):
    return render(request, "about.html", context={"team": team})


def features_page(request):
    return render(request, "features.html")


def notes_page(request):
    return render(request, "notes.html")


def help_page(request):
    return render(request, "help.html")


def fine_print_page(request):
    return render(request, "fine_print.html")


def guides_page(request):
    return render(request, "guides.html", context={"bg": rand_bg()})
