from os import path, listdir
from random import choice

from django.conf import settings

bg_root = (
    path.join(settings.STATICFILES_DIRS[0], "cou_site")
    if settings.DEBUG
    else path.join(settings.STATIC_ROOT)
)
bg_dir = path.join(bg_root, "img", "backgrounds")
backgrounds = None


def rand_bg():
    global backgrounds

    if not backgrounds:
        backgrounds = listdir(bg_dir)

    return choice(backgrounds)
