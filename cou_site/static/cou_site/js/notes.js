// Display
let loading = $('#note-loading');
let boardList = $('#note-list');

// Notes
let notes = [];
const bufferSize = 12;
let lastDisplayed = 0;

// Add the next set of notes_page to the board
function displayNotes() {
    $.each(notes.slice(lastDisplayed, lastDisplayed + bufferSize), function(i, noteData) {
        let note = $(document.createElement('li'));

        if (noteData.title.trim()) {
            let title = $(document.createElement('b'))
                .text(noteData.title);

            note
                .append(title)
                .append(document.createElement('br'));
        }

        let userLink = $(document.createElement('a'))
            .attr('href', '/profile?username=' + noteData.username)
            .text(noteData.username);

        note
            .append('Written by ')
            .append(userLink);

        let timestamp = $(document.createElement('span'))
            .text($.timeago(noteData.timestamp));

        note
            .append(' ')
            .append(timestamp);

        let streetLink = $(document.createElement('a'))
            .attr('href', '/encyclopedia/#/street/' + noteData.street_tsid)
            .text(noteData.street_label);

        note
            .append(' and left on ')
            .append(streetLink);

        boardList.append(note);
        lastDisplayed++;
    });
}

function fillScreen() {
    while (document.body.clientHeight <= document.documentElement.clientHeight) {
        // There is a blank space at the bottom of the page that will prevent scrolling
        // to load more notes_page, so load more now to fix this.
        displayNotes();
    }
}

// Download the notes_page from the server and display the first set
$.getJSON('https://server.childrenofur.com:8181/note/dropped', function(json) {
    loading.remove();
    notes = json.reverse();
    displayNotes();
    fillScreen();
});

// Display more sets when the page is scrolled down
$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
        displayNotes();
    }
});

// Display more notes_page if the page is too short to scroll
$(window).resize(function() {
    fillScreen();
});
