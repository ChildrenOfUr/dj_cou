from collections import namedtuple

from django.urls import resolve

NavigationPage = namedtuple("NavigationPage", ["name", "url"])

pages = [
    NavigationPage("About", "/about"),
    NavigationPage("Blog", "/blog"),
    NavigationPage("Forums", "https://forums.childrenofur.com/"),
    NavigationPage("Profiles", "/profiles"),
    NavigationPage("Notes", "/notes"),
    NavigationPage("Encyclopedia", "https://childrenofur.com/encyclopedia"),
    NavigationPage("Help", "/help"),
]


def navigation_processor(request):
    return {
        "pages": pages,
        "active_page": "/" + resolve(request.path_info).url_name
    }
